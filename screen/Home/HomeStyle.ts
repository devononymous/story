import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 1
    },
    inputFocus: {
        padding: 10,
        fontSize: 14,
        backgroundColor: 'grey',
        height: 43
      },
    textInput: {
        width: '90%',
        alignSelf: 'center',
        borderColor: 'grey',
        borderWidth: 1,
        borderRadius: 6,
        paddingVertical: 10,
        textAlign: 'center',
        marginTop: 70
    },
    button: {
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        backgroundColor: 'grey',
        borderRadius: 10,
        marginTop: 20
    },
    header: {
        flexDirection: 'row',
        position: 'absolute',
        top: 0,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 15,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        paddingVertical: 12,
        elevation: 5,
    }
})

export default styles
