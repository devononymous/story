import React, { useState, useEffect } from 'react'
import { SafeAreaView, Text, View, FlatList, TouchableOpacity } from 'react-native'
import { Searchbar, Button } from 'react-native-paper'

import { STORY_API } from '../../config/env.json'
import styles from './HomeStyle'

export default function Home({ navigation }: { navigation: any }) {

    const [country, setCountry] = useState(0)
    const [searchText, setSearchText] = useState('')
    const [allAsteroids, setAllAsteroids] = useState([] as any)
    const [filterBy, setFilterBy] = useState('')

    useEffect(() => {
        const timer = setInterval(() => {
            setCountry(country => country + 1)
            handleSubmit()
        }, 10000);
        return () => {
            clearInterval(timer)
        }
    }, [country])

    const handleSubmit = () => {

        fetch(STORY_API + country)
            .then(data => data.json())
            .then(value => {
                setAllAsteroids([...allAsteroids, ...value.hits])
            })
            .catch(err => console.log(err))
    }

    const handleLoadMore = () => {
        setCountry(country => country + 1)
        handleSubmit()
    }

    const searchData = () => {

        if (searchText.length === 0) {
            if (filterBy === 'title') return allAsteroids.sort((a: any, b: any) => a.title.toUpperCase() > b.title.toUpperCase())
            if (filterBy === 'createdAt') return allAsteroids.sort((a: any, b: any) => a.created_at_i < b.created_at_i)
            return allAsteroids
        }

        let tempArr = [] as any

        for (const i in allAsteroids) {
            if ((allAsteroids[i].title && allAsteroids[i].title.toUpperCase().includes(searchText.toUpperCase())) ||
                (allAsteroids[i].url && allAsteroids[i].url.toUpperCase().includes(searchText.toUpperCase())) ||
                (allAsteroids[i].author && allAsteroids[i].author.toUpperCase().includes(searchText.toUpperCase()))) {
                tempArr = [...tempArr, allAsteroids[i]]
                console.log('tempArr', tempArr.length)
                console.log('searchText', searchText)
            }
        }
        if (filterBy === 'title') return tempArr.sort((a: any, b: any) => a.title.toUpperCase() > b.title.toUpperCase())
        if (filterBy === 'createdAt') return tempArr.sort((a: any, b: any) => a.created_at_i < b.created_at_i)
        return tempArr
    }

    return (
        <SafeAreaView style={styles.container}>

            <Searchbar
                placeholder="Search by Title, Url, Author Name"
                onChangeText={(value) => setSearchText(value)}
                value={searchText}
                style={{ width: '90%', marginTop: 16 }}
                inputStyle={{ fontSize: 14 }}
            />

            <View style={{ flexDirection: 'row', justifyContent: 'space-around', width: '90%' }}>

                <Button
                    mode="contained"
                    onPress={() => setFilterBy('title')}
                    labelStyle={{ color: '#FFF', fontSize: 13, textTransform: 'capitalize' }}
                    style={styles.button}
                >
                    Filter By Title
                </Button>

                <Button
                    mode="contained"
                    onPress={() => setFilterBy('createdAt')}
                    labelStyle={{ color: '#FFF', fontSize: 13, textTransform: 'capitalize' }}
                    style={styles.button}
                >
                    Filter By Created At
                </Button>

            </View>

            <FlatList
                contentContainerStyle={{ marginTop: 20 }}
                onEndReached={handleLoadMore}
                onEndReachedThreshold={1}
                keyExtractor={(item) => item.objectID}
                data={searchData()}
                renderItem={({ item }) => {

                    return (
                        <TouchableOpacity
                            onPress={() => navigation.navigate('Detail', { item: item })}
                            style={{ marginBottom: 30, marginHorizontal: 16, borderBottomWidth: 0.6, borderBottomColor: 'grey', paddingBottom: 10 }}>

                            <Text style={{ fontWeight: '600', fontSize: 12, marginBottom: 4 }}>
                                <Text style={{ fontWeight: 'bold' }}>Title:</Text>  {item.title}
                            </Text>
                            <Text style={{ fontWeight: '600', fontSize: 12, marginBottom: 4 }}>
                                <Text style={{ fontWeight: 'bold' }}>URL:</Text>  {item.url}
                            </Text>
                            <Text style={{ fontWeight: '600', fontSize: 12, marginBottom: 4 }}>
                                <Text style={{ fontWeight: 'bold' }}>Created At:</Text>  {item.created_at}
                            </Text>
                            <Text style={{ fontWeight: '600', fontSize: 12 }}>
                                <Text style={{ fontWeight: 'bold' }}>Author:</Text>  {item.author}
                            </Text>

                        </TouchableOpacity>
                    )

                }}
            />

        </SafeAreaView>
    )
}
